```python
import random
import pandas as pd
from neo4j import GraphDatabase

NEO4J_URI='bolt://localhost:7687'
NEO4J_USER='neo4j'
NEO4J_PASS='neo4jPASS'
```


```python
query = """
       CALL gds.graph.project(
          'movieGraph',
          {
            Movie: {
            },
            Actor: {
            },
            Genre: {
            },
            Director: {
            }
          },{
            IN_GENRE: {
                orientation: 'UNDIRECTED'
            },
            ACTED_IN: {
                orientation: 'UNDIRECTED'
            },
            DIRECTED: {
                orientation: 'UNDIRECTED'
            }
          }
        )
       """
with GraphDatabase.driver(NEO4J_URI, auth=(NEO4J_USER, NEO4J_PASS)) as driver:
    query_result, _, _ = driver.execute_query(query)
    driver.close()

```


```python
query = """
            CALL gds.fastRP.stream.estimate('movieGraph', {embeddingDimension: 128})
            YIELD requiredMemory
            RETURN requiredMemory
            """

with GraphDatabase.driver(NEO4J_URI, auth=(NEO4J_USER, NEO4J_PASS)) as driver:
    query_result, _, _ = driver.execute_query(query)
    driver.close()

memory = [dict(record) for record in query_result]

print(f"Memory required to create embeddings: {memory[0]['requiredMemory']}")
```

    Memory required to create embeddings: 43 MiB
    


```python
random_seed = random.randint(0, 1000000)
query = f"""
        CALL gds.fastRP.stream('movieGraph', {{
          relationshipTypes: ['DIRECTED', 'ACTED_IN', 'IN_GENRE'],
          embeddingDimension: 128,
          randomSeed: {random_seed}
        }})
        YIELD nodeId, embedding
        MATCH (m:Movie)
        WHERE id(m) = nodeId
        RETURN nodeId, embedding
        """

with GraphDatabase.driver(NEO4J_URI, auth=(NEO4J_USER, NEO4J_PASS)) as driver:
    query_result, _, _ = driver.execute_query(query)
    driver.close()

embeddings = [dict(record) for record in query_result]

query = """
        UNWIND $embeddings AS row
        MATCH (n)
        where ID(n) = row.nodeId
        SET n.embedding = row.embedding
        return count(n) as analyzed_movies
        """

with GraphDatabase.driver(NEO4J_URI, auth=(NEO4J_USER, NEO4J_PASS)) as driver:
    query_result, _, _ = driver.execute_query(query, embeddings=embeddings)
    driver.close()

analyzed_movies = [dict(record) for record in query_result]
print(analyzed_movies)
```

    [{'analyzed_movies': 9125}]
    


```python
query = """
            MATCH (u:User)
            RETURN u.name as Name
            ORDER BY RAND()
            LIMIT 1
            """

with GraphDatabase.driver(NEO4J_URI, auth=(NEO4J_USER, NEO4J_PASS)) as driver:
    query_result, _, _ = driver.execute_query(query)
    driver.close()

results = [dict(record) for record in query_result]

name = results[0]["Name"]

query = f"""
            MATCH (user:User {{name: '{name}'}})-[watched:RATED]->(rated_movie:Movie)
            WHERE watched.rating >= 4
            WITH user, rated_movie, collect(DISTINCT rated_movie.embedding) as embeddings
            WITH rated_movie, REDUCE(s = [], e IN embeddings |
                [i IN RANGE(0, SIZE(e)-1) | COALESCE(s[i], 0.0) + e[i]]) AS sum,
                SIZE(embeddings) AS count
            with rated_movie, [x IN sum | x / count] AS averageEmbedding
            
            with averageEmbedding, rated_movie
            MATCH (other_movie:Movie)
            WHERE other_movie <> rated_movie
            WITH other_movie.title as recommendation, other_movie.embedding as otherEmbedding, averageEmbedding
            WITH recommendation, gds.similarity.cosine(averageEmbedding, otherEmbedding) AS similarity
            ORDER BY similarity DESC
            LIMIT 10
            RETURN recommendation, similarity
            """

with GraphDatabase.driver(NEO4J_URI, auth=(NEO4J_USER, NEO4J_PASS)) as driver:
    query_result, _, _ = driver.execute_query(query)
    driver.close()

movie_predictions = [dict(record) for record in query_result]

movie_df = pd.DataFrame(movie_predictions)
print(name, movie_df)
```

    Alexis Steele MD                    recommendation  similarity
    0                  Grumpy Old Men    0.858187
    1                   Jurassic Park    0.808978
    2              Odd Couple II, The    0.804159
    3             Fortune Cookie, The    0.797590
    4                 Front Page, The    0.786504
    5  Lost World: Jurassic Park, The    0.786379
    6            Bourne Identity, The    0.778431
    7                            I.Q.    0.777085
    8                      Out to Sea    0.767656
    9                           Chaos    0.766575
    


```python

```
